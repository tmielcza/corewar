/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/19 18:57:58 by gjestin           #+#    #+#             */
/*   Updated: 2014/02/02 22:34:04 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#ifndef ASM_H
# define ASM_H

# include <fcntl.h>
# include <unistd.h>
# include <stdlib.h>
# include <stdio.h>
# include "libft.h"
# include "op.h"

# define T_ALL (T_REG | T_IND | T_DIR)
# define LABEL_PREF {DIRECT_CHAR, LABEL_CHAR, '\0'}
# define DIRECT_PREF {DIRECT_CHAR, '\0'}
# define NB_ERRORS	9
# define NB_FERROR	2
# define NAME		1
# define COMMENT	0

enum	e_ferror
{
	badusage,
	noopen
};

enum	e_error
{
	noerr,
	fewarg,
	manyarg,
	wrargtype,
	badinstr,
	secdeclab,
	badmalloc,
	nolabel,
	badsyn
};

typedef struct		s_op
{
	char			name[6];
	int				n_arg;
	int				params[3];
	int				code;
	int				cycles;
	int				p_coding;
	int				carry;
	int				t_dir_size;
}					t_op;

typedef struct			s_label
{
	struct s_label		*next;
	int					addr;
	char				*name;
}						t_label;

typedef struct			s_instr
{
	t_op		op;
	char		*arg[4];
}						t_instr;

/*
** cw_anal_yzer.c (3 statics)
*/
int			cw_parser(int fd, header_t *bin, t_label *l_labs);

/*
** cw_ass_embler.c (4 statics)
*/
void		cw_ass_embler(int fd, char *file);

/*
** cw_verif.c
*/
char		cw_verif_type(t_op hop, char *line, int arg);
int			cw_verif_arg(t_instr *i, char *line, int arg);
int			cw_verif_args(t_instr *instr, t_op hop, char *line);
int			cw_pars_line(t_instr *instr, char *line);

/*
** cw_labelizer.c
*/
int			cw_is_label_char(char c);
int			cw_is_label(char *c);
char		*cw_labelizer(int size, char *line, t_label *curr);
int			cw_get_label_addr(char *name, t_label *l_labs);
t_label		*cw_add_link(t_label *prev, int size, char *name);

/*
** cw_free.c
*/
void		cw_free_list(t_label *list);

/*
** cw_put_header.c (2 static)
*/
void		cw_put_header(int fd_out, header_t bin);

/*
** cw_put_params.c (1 static)
*/
int			cw_put_params(t_instr *i, char *bytecode, t_label *l, int size);

/*
** cw_generics.c
*/
int			cw_is_digit(char c);
char		*cw_skip_spaces(char *line);
int			cw_wordlen(char *line);

/*
** cw_error.c
*/
int			cw_ferror(enum e_ferror error);
int			cw_error(enum e_error error, char *line);

/*
** cw_to_big_endan.c
*/
int			cw_to_big_endian(int val);
int			cw_2_big_endian(int val);

/*
** op.c
*/
t_op		*cw_get_op_tab(void);

#endif /* !ASM_H */
