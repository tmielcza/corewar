/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_descriptor_u.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/16 18:54:17 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/25 19:39:03 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>

int		ft_descriptor_u(va_list ap, t_printp param)
{
	unsigned int	n;
	char			*s;

	(void)param;
	param.plus = 0;
	n = va_arg(ap, unsigned int);
	s = ft_itoa_unsigned(n);
	ft_putstr(s);
	n = ft_strlen(s);
	free(s);
	return (n - 2);
}
