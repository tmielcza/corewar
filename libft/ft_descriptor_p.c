/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_descriptor_p.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/17 19:46:34 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/26 18:04:41 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static void		ft_print_hex(unsigned long n, int *total)
{
	if (n != 0)
	{
		ft_print_hex(n / 16, total);
		n %= 16;
		if (n > 9)
			ft_putchar(n + 87);
		else
			ft_putnbr(n % 16);
		*total = *total + 1;
	}
}

int				ft_descriptor_p(va_list ap, t_printp param)
{
	unsigned long	n;
	int				total;
	int				*ptotal;

	(void)param;
	param.plus = 0;
	total = 0;
	ptotal = &total;
	n = va_arg(ap, unsigned long);
	ft_putstr("0x");
	if (n == 0)
	{
		ft_putnbr(0);
		return (1);
	}
	ft_print_hex(n, ptotal);
	return (total);
}
