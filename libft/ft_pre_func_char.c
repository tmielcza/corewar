/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pre_func_char.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/18 15:21:34 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/25 19:43:36 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int		ft_check_param(char *fmt, int *offset, t_printp *param)
{
	int		total;

	total = 0;
	if (fmt[*offset] == '#' || fmt[*offset] == '-' || fmt[*offset] == '+'
		|| fmt[*offset] == '0' || fmt[*offset] == '*')
	{
		if (fmt[*offset] == '+')
			param->plus = 1;
		if (fmt[*offset] == '-')
			param->minus = 1;
		if (fmt[*offset] == '0')
			param->zero = 1;
		if (fmt[*offset] == '#')
			param->sharp = 1;
		if (fmt[*offset] == '*')
			param->star = 1;
		*offset = *offset + 1;
		total--;
	}
	return (total);
}

int		ft_pre_func_char(char *fmt, int *offset, t_printp *param)
{
	int		total;

	total = 0;
	if (fmt[*offset] == ' ')
	{
		ft_putchar(fmt[*offset]);
		*offset = *offset + 1;
	}
	total += ft_check_param(fmt, offset, param);
	if (ft_isdigit(fmt[*offset]))
		total += ft_compute_gap(fmt, offset, param);
	return (total);
}
