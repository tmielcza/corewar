/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 21:16:47 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/30 20:19:55 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>

size_t			ft_strlen(const char *s);
char			*ft_strncpy(const char *s1, const char *s2, size_t n);

static size_t	ft_countlen(const char *s, char c)
{
	size_t	size;

	size = 0;
	while (*s && *s != c)
	{
		size++;
		s++;
	}
	return (size);
}

static size_t	ft_countstr(const char *s, char c)
{
	size_t	n;
	int		add;

	add = 1;
	n = 0;
	while (*s)
	{
		if (*s == c)
			add = 1;
		else if (*s != c && add)
		{
			n++;
			add = 0;
		}
		s++;
	}
	return (n);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**ret;
	char	*tmp;
	int		i;
	int		len;

	ret = (char **)malloc(sizeof(char *) * (ft_countstr(s, c) + 1));
	i = 0;
	while (*s)
	{
		if (*s != c)
		{
			len = ft_countlen(s, c);
			tmp = (char *)malloc(sizeof(char) * (ft_countlen(s, c) + 1));
			tmp = ft_strncpy(tmp, s, len);
			tmp[len] = '\0';
			ret[i] = tmp;
			i++;
			s += ft_strlen(tmp) - 1;
		}
		s++;
	}
	ret[i] = '\0';
	return (ret);
}
