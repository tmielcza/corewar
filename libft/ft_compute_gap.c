/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_compute_gap.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/18 18:27:51 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/25 19:43:16 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

int		ft_compute_gap(char *fmt, int *offset, t_printp *param)
{
	int		total;
	char	*s;

	total = 0;
	param->len = ft_atoi(fmt + *offset);
	s = ft_itoa(param->len);
	*offset = *offset + ft_strlen(s);
	total -= ft_strlen(s);
	return (total);
}
