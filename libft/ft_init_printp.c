/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_printp.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/18 16:10:55 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/25 19:42:54 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdarg.h>

void	ft_init_printp(t_printp *p)
{
	p->plus = 0;
	p->minus = 0;
	p->zero = 0;
	p->sharp = 0;
	p->star = 0;
	p->len = 0;
}
