/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_descriptor_o.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/16 19:20:16 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/25 19:41:53 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static void		ft_print_oct(unsigned int n, int *total)
{
	if (n != 0)
	{
		ft_print_oct(n / 8, total);
		ft_putnbr(n % 8);
		*total = *total + 1;
	}
}

int				ft_descriptor_o(va_list ap, t_printp param)
{
	unsigned int	n;
	int				total;
	int				*ptotal;

	total = 0;
	ptotal = &total;
	n = va_arg(ap, unsigned int);
	if (n == 0)
	{
		ft_putnbr(0);
		return (-1);
	}
	if (param.sharp)
	{
		ft_putchar('0');
		total++;
	}
	ft_print_oct(n, ptotal);
	return (total - 2);
}
