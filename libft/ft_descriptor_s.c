/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_descriptor_s.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/15 18:23:38 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/25 19:38:45 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdarg.h>
#include <unistd.h>

int		ft_descriptor_s(va_list ap, t_printp param)
{
	char	*s;
	int		len;

	(void)param.plus;
	s = va_arg(ap, char *);
	if (s == NULL)
	{
		write(1, "(null)", 6);
		return (4);
	}
	len = ft_strlen(s);
	write(1, s, len);
	return (len - 2);
}
