/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_descriptor_d.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/15 16:22:18 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/25 19:38:04 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>
#include <stdarg.h>

void	ft_putdiff(int diff, int *total)
{
	while (diff > 0)
	{
		ft_putchar(' ');
		*total = *total + 1;
		diff--;
	}
}

char	*ft_manage_star(va_list ap, t_printp *param, int *diff)
{
	char	*s;

	if (!param->star)
	{
		s = ft_itoa(va_arg(ap, int));
		*diff = param->len - ft_strlen(s);
	}
	else
	{
		*diff = va_arg(ap, int);
		s = ft_itoa(va_arg(ap, int));
		if (*diff < 0)
		{
			param->minus = 1;
			*diff = -*diff;
		}
		*diff = *diff - ft_strlen(s);
	}
	return (s);
}

int		ft_descriptor_d(va_list ap, t_printp param)
{
	char	*s;
	int		total;
	int		diff;

	total = -2;
	s = ft_manage_star(ap, &param, &diff);
	if (!param.minus)
		ft_putdiff(diff, &total);
	if (s[0] != '-' && param.plus)
	{
		ft_putchar('+');
		total++;
	}
	ft_putstr(s);
	if (param.minus)
		ft_putdiff(diff, &total);
	free(s);
	return (ft_strlen(s) + total);
}
