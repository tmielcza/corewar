/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 16:13:15 by gjestin           #+#    #+#             */
/*   Updated: 2013/11/23 17:27:54 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strstr(const char *s1, const char *s2)
{
	int		i;
	int		j;

	if (*s2 == '\0')
		return ((char *)s1);
	i = 0;
	j = 0;
	while (s1[i] != '\0')
	{
		while (s1[i + j] != '\0' && s1[i + j] == s2[j])
		{
			j++;
		}
		if (s2[j] == '\0')
			return ((char *)s1 + i);
		j = 0;
		i++;
	}
	return (NULL);
}
