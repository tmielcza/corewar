/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 15:52:59 by gjestin           #+#    #+#             */
/*   Updated: 2014/01/28 16:15:12 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdlib.h>
#include <stdio.h>
#include "libft.h"

t_list		*ft_lstmap(t_list *lst, t_list *(*f)(t_list *elem))
{
	t_list	*newlst;
	t_list	*newelem;
	t_list	*endpos;

	newlst = (t_list *)malloc(sizeof(t_list));
	newlst = (*f)(lst);
	endpos = newlst;
	newelem = (t_list *)malloc(sizeof(t_list));
	newelem = (*f)(lst);
	endpos->next = newelem;
	return (newlst);
}
