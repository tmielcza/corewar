/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_get_next_line.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/17 16:15:03 by gjestin           #+#    #+#             */
/*   Updated: 2014/01/08 20:09:03 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdlib.h>
#include <unistd.h>

static size_t	count_size(char *s)
{
	size_t	n;

	n = 0;
	while (s[n] && s[n] != '\n')
		n++;
	return (n);
}

static int		build_line(char **line, char *excess)
{
	size_t	n;

	n = count_size(excess);
	*line = ft_strnew(n + 1);
	*line = ft_strncpy(*line, excess, n);
	(*line)[n] = '\0';
	excess = ft_strcpy(excess, excess + n + 1);
	return (1);
}

int				ft_end_file(char **line, char **excess)
{
	*line = ft_strdup(*excess);
	free(*excess);
	*excess = NULL;
	return (0);
}

int				ft_get_next_line(int fd, char **line)
{
	static char	*excess;
	int			ret;
	char		*buf;

	if (excess && ft_strchr(excess, '\n'))
		return (build_line(line, excess));
	else
	{
		buf = ft_strnew(BUFF_SIZE + 1);
		while (!ft_strchr(buf, '\n') && (ret = read(fd, buf, BUFF_SIZE)))
		{
			if (ret == -1)
				return (-1);
			buf[ret] = '\0';
			excess = excess != NULL ? ft_strjoin(excess, buf) : ft_strdup(buf);
		}
		free(buf);
		if (!ret)
			return (ft_end_file(line, &excess));
		return (build_line(line, excess));
	}
}
