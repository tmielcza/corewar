/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 10:08:37 by gjestin           #+#    #+#             */
/*   Updated: 2013/11/21 11:05:01 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memccpy(void *s1, const void *s2, int c, size_t n)
{
	unsigned char	*p1;
	unsigned char	*p2;

	p1 = s1;
	p2 = (unsigned char *)s2;
	while (n > 0)
	{
		*p1 = *p2;
		if (*p2 == (unsigned char)c)
			return (p1 + 1);
		p1++;
		p2++;
		n--;
	}
	return ((void *)0);
}
