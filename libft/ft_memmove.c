/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 10:32:16 by gjestin           #+#    #+#             */
/*   Updated: 2013/11/21 19:04:35 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include <stdlib.h>

void	*ft_memcpy(void *s1, const void *s2, size_t n);

void	*ft_memmove(void *s1, const void *s2, size_t n)
{
	char	*tmp;
	char	*p1;
	char	*p2;

	tmp = (char *)malloc(sizeof(char) * n);
	p1 = s1;
	p2 = (char *)s2;
	ft_memcpy(tmp, p2, n);
	ft_memcpy(p1, tmp, n);
	free(tmp);
	return (s1);
}
