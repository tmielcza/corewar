/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/11 22:13:45 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/25 19:28:35 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdarg.h>

int		ft_printf(char *format, ...)
{
	va_list		ap;
	int			i;
	int			total;

	va_start(ap, format);
	i = 0;
	total = 0;
	while (format[i] != '\0')
	{
		if (format[i] != '%')
			ft_putchar(format[i]);
		else
		{
			i++;
			total += ft_eval_func(ap, format, &i);
		}
		i++;
	}
	va_end(ap);
	return (i + total);
}
