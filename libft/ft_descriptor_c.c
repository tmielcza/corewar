/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_descriptor_c.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/15 18:38:59 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/25 19:38:25 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdarg.h>

int		ft_descriptor_c(va_list ap, t_printp param)
{
	param.plus = 0;
	(void)param;
	ft_putchar((char)va_arg(ap, int));
	return (-1);
}
