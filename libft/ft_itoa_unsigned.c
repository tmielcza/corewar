/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa_unsigned.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/31 18:13:06 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/31 18:13:10 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>

static int	ft_get_len(unsigned int n)
{
	size_t	len;

	len = 0;
	while (n > 9)
	{
		len++;
		n /= 10;
	}
	len++;
	return (len);
}

char		*ft_itoa_unsigned(unsigned int n)
{
	char	*ret;
	size_t	len;

	len = ft_get_len(n);
	ret = (char *)malloc(sizeof(char) * len);
	if (!ret)
		return (NULL);
	ret[len] = '\0';
	len--;
	if (n == 0)
		ret[len] = '0';
	while (n != 0)
	{
		ret[len] = n % 10 + '0';
		n /= 10;
		len--;
	}
	return (ret);
}
