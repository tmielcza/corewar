/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/28 16:23:18 by tmielcza          #+#    #+#             */
/*   Updated: 2014/02/02 22:20:31 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "libft.h"

void			ft_putnbr(long long int n)
{
	long long int	n2;
	int				z;
	char			*mem;

	n2 = n;
	z = 0;
	while (n2 > 9 || n2 < (-9))
	{
		n2 /= 10;
		z++;
	}
	n2 = n < 0 ? 1 : 0;
	if ((mem = ft_strnew(z * sizeof(char) + n2 + 1)))
	{
		mem[0] = n < 0 ? '-' : mem[0];
		while (z >= 0)
		{
			mem[n2 + z--] = '0' + ft_abs(n % 10);
			n /= 10;
		}
	}
	ft_putstr(mem);
}
