/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_descriptor_x.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/17 18:48:12 by gjestin           #+#    #+#             */
/*   Updated: 2013/12/25 19:42:10 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>

static void		ft_print_hex(unsigned int n, int *total)
{
	if (n != 0)
	{
		ft_print_hex(n / 16, total);
		n %= 16;
		if (n > 9)
			ft_putchar(n + 87);
		else
			ft_putnbr(n % 16);
		*total = *total + 1;
	}
}

int				ft_descriptor_x(va_list ap, t_printp param)
{
	unsigned int	n;
	int				total;
	int				*ptotal;

	total = 0;
	ptotal = &total;
	n = va_arg(ap, unsigned int);
	if (n == 0)
	{
		ft_putnbr(0);
		return (-1);
	}
	if (param.sharp)
	{
		ft_putstr("0x");
		total += 2;
	}
	ft_print_hex(n, ptotal);
	return (total - 2);
}
