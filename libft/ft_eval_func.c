/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eval_func.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/16 18:20:04 by gjestin           #+#    #+#             */
/*   Updated: 2014/02/02 22:36:26 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <libft.h>
#include <stdarg.h>

int     (*f[N_FUNC])(va_list, t_printp) = {ft_descriptor_d,
										   ft_descriptor_d,
										   ft_descriptor_c,
										   ft_descriptor_s,
										   ft_descriptor_u,
										   ft_descriptor_o,
										   ft_descriptor_x,
										   ft_descriptor_p};
char    descr[] = "dicsuoxp";

int		ft_eval_func(va_list ap, char *fmt, int *offset)
{
	int			j;
	int			total;
	t_printp	param;

	ft_init_printp(&param);
	total = ft_pre_func_char(fmt, offset, &param);
	j = 0;
	while (j < N_FUNC)
	{
		if (fmt[*offset] == descr[j])
			return (total + (*f[j])(ap, param));
		j++;
	}
	if (fmt[*offset] == '%')
	{
		ft_putchar('%');
		return (total - 1);
	}
	else
	{
		ft_putchar(fmt[*offset]);
		return (total - 1);
	}
}
