/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/21 20:42:03 by gjestin           #+#    #+#             */
/*   Updated: 2013/11/24 18:30:07 by gjestin          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <string.h>

size_t			ft_strlen(const char *s);

static size_t	ft_trimstartlen(const char *s)
{
	size_t	head;

	head = 0;
	while (s[head] == ' ' || s[head] == '\n' || s[head] == '\t')
		head++;
	return (head);
}

static size_t	ft_trimendlen(const char *s)
{
	size_t	tail;
	size_t	len;

	len = ft_strlen(s) - 1;
	tail = 0;
	while (s[len] == ' ' || s[len] == '\n' || s[len] == '\t')
	{
		len--;
		tail++;
	}
	return (tail);
}

char			*ft_strtrim(char const *s)
{
	size_t	head;
	size_t	tail;
	size_t	size;
	char	*ret;

	head = ft_trimstartlen(s);
	tail = ft_trimendlen(s);
	size = ft_strlen(s) - head - tail;
	ret = (char *)malloc(sizeof(char) * size);
	if (!ret)
		return (NULL);
	tail = 0;
	while (tail < size)
	{
		ret[tail] = s[head + tail];
		tail++;
	}
	ret[tail] = '\0';
	return (ret);
}
