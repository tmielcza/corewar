/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/26 14:22:57 by gjestin           #+#    #+#             */
/*   Updated: 2014/01/28 16:14:47 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <stdlib.h>
#include "libft.h"

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*lst;

	lst = *alst;
	while (lst->next != NULL)
	{
		del(lst->content, lst->content_size);
		lst = lst->next;
	}
	del(lst->content, lst->content_size);
	free(*alst);
	*alst = NULL;
}
