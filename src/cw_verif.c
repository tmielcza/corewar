/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cw_verif.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/02 22:30:54 by tmielcza          #+#    #+#             */
/*   Updated: 2014/02/02 22:33:09 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

char		cw_verif_type(t_op hop, char *line, int arg)
{
	char	tab[] = {'r', DIRECT_CHAR, '\0'};
	char	flags[] = {T_REG, T_DIR, 0, T_IND};
	char	codes[] = {REG_CODE, DIR_CODE, 0, IND_CODE};
	int		i;

	i = 0;
	while (i < 3 && tab[i] != *line)
		i++;
	if (!flags[i])
		return (0);
	else if (!(flags[i] & hop.params[arg]))
		return (cw_error(wrargtype, line));
	return (codes[i]);
}

int			cw_verif_arg(t_instr *i, char *line, int arg)
{
	char	prefix[4][3] = {"r", LABEL_PREF, DIRECT_PREF, ""};
	int		j;
	int		(*f)(char);
	int		k;

	j = k = 0;
	while (ft_strncmp(prefix[j], line, ft_strlen(prefix[j])))
		j++;
	f = j == 1 ? cw_is_label_char : cw_is_digit;
	j = ft_strlen(prefix[j]);
	line += j;
	while (line[k] && line[k] != ' ' && line[k] != '\t'
		   && line[k] != SEPARATOR_CHAR)
	{
		if (!f(line[k]))
		{
			cw_error(badsyn, line);
			return (0);
		}
		k++;
	}
	i->arg[arg + 1] = ft_strnew(k + j);
	ft_bzero(i->arg[arg + 1], k + j + 1);
	ft_strncpy(i->arg[arg + 1], line - j, k + j);
	return (1);
}

int			cw_verif_args(t_instr *instr, t_op hop, char *line)
{
	int		i;

	instr->op = hop;
	i = -1;
	while (++i < hop.n_arg)
	{
		line = cw_skip_spaces(line);
		if (!(cw_verif_type(hop, line, i)))
			cw_error(fewarg, line);
		if (!cw_verif_arg(instr, line, i))
			return (0);
		while (*line && *line != ' ' && *line != '\t'
			   && *line != SEPARATOR_CHAR)
			line++;
		line = cw_skip_spaces(line);
		if (!(i == hop.n_arg - 1) && *line == SEPARATOR_CHAR)
			line++;
	}
	return (1);
}

int			cw_pars_line(t_instr *instr, char *line)
{
	t_op		*tab;
	int			i;

	i = 15;
	tab = cw_get_op_tab();
	if (!(*line) || !ft_strncmp(NAME_CMD_STRING, line, 5) ||
		!ft_strncmp(COMMENT_CMD_STRING, line, 8) || *line == COMMENT_CHAR)
		return (0);
	while (--i >= 0 && (ft_strncmp(line, tab[i].name, cw_wordlen(tab[i].name))))
		;
	if (i == -1)
	{
		cw_error(badinstr, line);
		return (-1);
	}
	instr->op = tab[i];
	instr->arg[0] = ft_strnew(cw_wordlen(line));
	ft_bzero(instr->arg[0], cw_wordlen(line));
	ft_strncpy(instr->arg[0], line, cw_wordlen(line));
	if (!cw_verif_args(instr, tab[i], line + cw_wordlen(line)))
		return (-1);
	return (1);
}
