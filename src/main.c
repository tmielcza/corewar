/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/19 18:57:05 by gjestin           #+#    #+#             */
/*   Updated: 2014/02/02 17:17:07 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int		main(int ac, char **av)
{
	int		fd;
	char	*ret;

	if (ac != 2 || !(ret = ft_strrchr(av[1], '.')) || ft_strcmp(".s", ret))
		cw_ferror(badusage);
	if ((fd = open(av[1], O_RDONLY)) < 0)
		cw_ferror(noopen);
	ft_printf("Assembling %s:\n", av[1]);
	cw_ass_embler(fd, av[1]);
	close(fd);
	return (0);
}
