/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cw_ass_embler.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/30 21:53:42 by tmielcza          #+#    #+#             */
/*   Updated: 2014/02/02 22:33:42 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static int	cw_put_coding_byte(t_instr *instr, char *bcode, int size)
{
	char	code;
	int		i;
	int		j;
	char	argcode;

	j = -1;
	code = 0;
	i = 6;
	while (++j < instr->op.n_arg)
	{
		argcode = cw_verif_type(instr->op, instr->arg[j + 1], j) << i;
		code |= argcode;
		i -= 2;
	}
	ft_memcpy(bcode + size, &code, 1);
	return (1);
}

static char	*cw_prepare_name(char *filein)
{
	char	*fileout;
	size_t	len;

	fileout = NULL;
	len = ft_strlen(filein) + 3;
	fileout = (char *)malloc(sizeof(char) * len);
	fileout = ft_strcpy(fileout, filein);
	ft_strcpy(fileout + len - 4, "cor");
	return (fileout);
}

static int	cw_put_instr(char *line, char *bytecode, t_label *l_labs, int size)
{
	int			i;
	char		val;
	t_instr		instr;

	i = 16;
	if (cw_pars_line(&instr, line) <= 0)
		return (size);
	val = instr.op.code;
	ft_memcpy(bytecode + size, &val, 1);
	size++;
	line += ft_strlen(instr.op.name);
	if (instr.op.p_coding)
		size += cw_put_coding_byte(&instr, bytecode, size);
	size = cw_put_params(&instr, bytecode, l_labs, size);
	return (size);
}

static void	cw_write_code(header_t bin, char *bytecode, char *file)
{
	int		fd_out;
	char	*fileout;

	fileout = cw_prepare_name(file);
	if ((fd_out = open(fileout, O_WRONLY | O_CREAT | O_TRUNC, 00644)) < 0)
	{
		free(fileout);
		free(bytecode);
		cw_ferror(noopen);
	}
	ft_printf("name: %s\n", bin.prog_name);
	ft_printf("comment: %s\n", bin.comment);
	cw_put_header(fd_out, bin);
	write(fd_out, bytecode, bin.prog_size);
	free(fileout);
	close(fd_out);
}

void		cw_ass_embler(int fd, char *file)
{
	header_t	bin;
	t_label		*l_labs;
	char		*line;
	int			size;
	char		*bytecode;

	l_labs = NULL;
	size = 0;
	l_labs = cw_add_link(NULL, 0, "START");
	cw_parser(fd, &bin, l_labs);
	lseek(fd, 0, SEEK_SET);
	if (!(bytecode = (char *)malloc(sizeof(char) * bin.prog_size)))
		cw_error(badmalloc, NULL);
	while (ft_get_next_line(fd, &line) > 0 && line)
	{
		if (*line && *line != '#' && !cw_is_label(line))
			size = cw_put_instr(cw_skip_spaces(line), bytecode, l_labs, size);
		free(line);
	}
	cw_free_list(l_labs);
	if (!cw_error(noerr, NULL))
		cw_write_code(bin, bytecode, file);
}
