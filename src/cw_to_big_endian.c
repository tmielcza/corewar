/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cw_to_big_endian.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/30 21:54:44 by tmielcza          #+#    #+#             */
/*   Updated: 2014/01/30 21:55:30 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int		cw_to_big_endian(int val)
{
	int		ret;
	char	*cur;
	char	*cur_ret;

	cur = (char *)&val;
	cur_ret = (char *)&ret;
	*cur_ret = *(cur + 3);
	*(cur_ret + 1) = *(cur + 2);
	*(cur_ret + 2) = *(cur + 1);
	*(cur_ret + 3) = *cur;
	return (ret);
}

int		cw_2_big_endian(int val)
{
	int		ret;
	char	*cur;
	char	*cur_ret;

	cur = (char *) &val;
	cur_ret = (char *) &ret;
	*(cur_ret + 1) = *cur;
	*cur_ret = *(cur + 1);
	return (ret);
}
