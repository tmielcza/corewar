/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cw_free.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/01 11:41:56 by tmielcza          #+#    #+#             */
/*   Updated: 2014/02/02 22:25:52 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

void		cw_free_list(t_label *list)
{
	t_label	*tmp;

	while (list)
	{
		tmp = list->next;
		free(list->name);
		free(list);
		list = tmp;
	}
}
