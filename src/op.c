/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   op.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: zaz <marvin@42.fr>                         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/10/04 11:43:01 by zaz               #+#    #+#             */
/*   Updated: 2014/01/30 22:17:24 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

/*
**  0		1			2		  3			4		  5			 6		  7
** NAME | PARAM NB | PARAMS T | OPCODE | CYCLES | CODING OCT | CARRY | DIR_SIZE
*/

t_op			*cw_get_op_tab(void)
{
	static t_op		*ret;
	static t_op		op_tab[17] =
	{
		{"live", 1, {T_DIR}, 1, 10, 0, 0, 4},
		{"ld", 2, {T_DIR | T_IND, T_REG}, 2, 5, 1, 0, 4},
		{"st", 2, {T_REG, T_IND | T_REG}, 3, 5, 1, 0, 0},
		{"add", 3, {T_REG, T_REG, T_REG}, 4, 10, 1, 0, 0},
		{"sub", 3, {T_REG, T_REG, T_REG}, 5, 10, 1, 0, 0},
		{"and", 3, {T_ALL, T_ALL, T_REG}, 6, 6, 1, 0, 4},
		{"or", 3, {T_ALL, T_ALL, T_REG}, 7, 6, 1, 0, 4},
		{"xor", 3, {T_ALL, T_ALL, T_REG}, 8, 6, 1, 0, 4},
		{"zjmp", 1, {T_DIR}, 9, 20, 0, 1, 2},
		{"ldi", 3, {T_ALL, T_DIR | T_REG, T_REG}, 10, 25, 1, 1, 2},
		{"sti", 3, {T_REG, T_ALL, T_DIR | T_REG}, 11, 25, 1, 1, 2},
		{"fork", 1, {T_DIR}, 12, 800, 0, 1, 2},
		{"lld", 2, {T_DIR | T_IND, T_REG}, 13, 10, 1, 0, 4},
		{"lldi", 3, {T_ALL, T_DIR | T_REG, T_REG}, 14, 50, 1, 1, 2},
		{"lfork", 1, {T_DIR}, 15, 1000, 0, 1, 2},
		{"aff", 1, {T_REG}, 16, 2, 1, 0, 0},
		{"", 0, {0}, 0, 0, 0, 0, 0}
	};

	ret = op_tab;
	return (ret);
}
