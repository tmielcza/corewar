/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cw_error.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/28 17:30:16 by tmielcza          #+#    #+#             */
/*   Updated: 2014/02/02 17:16:30 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include "asm.h"

int			cw_ferror(enum e_ferror error)
{
	static char		error_tab[NB_FERROR][30] =
	{
		"Usage: ./asm file_name [...]", "Error: "
	};

	if (error == noopen)
		perror(error_tab[error]);
	else
		ft_printf("%s\n", error_tab[error]);
	exit(1);
}

int			cw_error(enum e_error error, char *line)
{
	static int		is_error;
	static char		error_tab[NB_ERRORS][30] =
	{
		"", "Too few arguments", "Too many arguments", "Wrong argument type",
		"Unknown instruction", "Already declared label", "Malloc error",
		"Unknown label name", "Syntax error"
	};

	if (!error)
		return (is_error);
	is_error = 1;
	ft_printf("Cumpilation Error : %s\n", error_tab[error]);
	if (line)
		ft_printf("%s\n\n", line);
	return (-1);
}
