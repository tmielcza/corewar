/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cw_anal_yzer.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/22 19:09:28 by tmielcza          #+#    #+#             */
/*   Updated: 2014/02/02 22:22:44 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static int	cw_add_param_bytes(char **line, t_op *instr)
{
	int		cont;
	int		size;

	size = 0;
	cont = instr->n_arg;
	while (cont)
	{
		*line = cw_skip_spaces(*line);
		size += **line == 'r' ? 1 : 0;
		size += **line == DIRECT_CHAR ? instr->t_dir_size : 0;
		size += **line != 'r' && **line != DIRECT_CHAR ? 2 : 0;
		while (**line && **line != SEPARATOR_CHAR && **line != COMMENT_CHAR)
			(*line)++;
		cont -= **line == SEPARATOR_CHAR ? 1 : cont;
		(*line)++;
	}
	return (size);
}

static int	cw_check_line(char *line)
{
	int				i;
	unsigned int	size;
	t_op			*tab;
	char			tempchar;

	tab = cw_get_op_tab();
	size = 0;
	i = 15;
	tempchar = *(line + cw_wordlen(line));
	*(line + cw_wordlen(line)) = '\0';
	while (i >= 0 && ft_strncmp(line, tab[i].name, cw_wordlen(line) + 1))
		i--;
	*(line + cw_wordlen(line)) = tempchar;
	if (i != -1)
	{
		size += tab[i].p_coding ? 1 : 0;
		line += ft_strlen(tab[i].name);
		size += cw_add_param_bytes(&line, tab + i);
		return (size + 1);
	}
	else if (*line != COMMENT_CHAR)
		cw_error(badinstr, line);
	return (0);
}

static int	cw_check_header(char **line, header_t *bin)
{
	char	*str;
	int		len[2] = {ft_strlen(NAME_CMD_STRING),
					  ft_strlen(COMMENT_CMD_STRING)};
	char	*ret;

	str = NULL;
	if (**line && !ft_strncmp(*line, NAME_CMD_STRING, len[0]))
		str = bin->prog_name;
	else if (**line && !ft_strncmp(*line, COMMENT_CMD_STRING, len[1]))
		str = bin->comment;
	if (!str)
		return (0);
	if ((ret = ft_strrchr(*line, '"')))
		*ret = '\0';
	if (!ret || !(ret = ft_strchr(*line, '"')))
		cw_error(badsyn, *line);
	ft_strcpy(str, ret + 1);
	return (1);
}

int			cw_parser(int fd, header_t *bin, t_label *l_labs)
{
	char	*line;
	char	*linetofree;

	bin->prog_size = 0;
	while (ft_get_next_line(fd, &line))
	{
		linetofree = line;
		if (!(cw_check_header(&line, bin)))
		{
			if (cw_is_label(line))
				line = cw_labelizer(bin->prog_size, line, l_labs);
			if (*(line = cw_skip_spaces(line)))
				bin->prog_size += cw_check_line(line);
		}
	}
	if (line)
		free(linetofree);
	return (0);
}
