/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cw_put_params.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/30 22:07:30 by tmielcza          #+#    #+#             */
/*   Updated: 2014/02/02 22:29:00 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static int	cw_put(char *bcode, int param, int len, int size)
{
	param = len == 2 ? cw_2_big_endian(param) : param;
	param = len == 4 ? cw_to_big_endian(param) : param;
	ft_memcpy(bcode + size, &param, len);
	return (len);
}

int			cw_put_params(t_instr *i, char *bcode, t_label *l_labs, int ttl_z)
{
	int		param;
	char	prefix[4][3] = {"r", LABEL_PREF, DIRECT_PREF, ""};
	int		sizes[5] = {1, i->op.t_dir_size, i->op.t_dir_size, IND_SIZE, 0};
	int		j;
	int		k;

	k = 1;
	while ((i->op.n_arg)--)
	{
		j = 0;
		while (ft_strncmp(prefix[j], i->arg[k], ft_strlen(prefix[j])))
				j++;
		if (j == 1)
		{
			param = cw_get_label_addr(i->arg[k] + 2, l_labs);
			param -= ttl_z - i->op.p_coding;
		}
		else
			param = ft_atoi(i->arg[k] + ft_strlen(prefix[j]));
		sizes[4] += cw_put(bcode, param, sizes[j], ttl_z + sizes[4]);
		k++;
	}
	return (ttl_z + sizes[4]);
}
