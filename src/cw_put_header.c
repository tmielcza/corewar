/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cw_put_header.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/30 21:48:14 by tmielcza          #+#    #+#             */
/*   Updated: 2014/02/02 22:28:11 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static int		cw_write_chars(char *str, int fd_out)
{
	int		i;

	i = 0;
	while (str[i])
		i++;
	return (write(fd_out, str, i));
}

static void		cw_put_info(int fd_out, char *info, int m)
{
	int		i;
	int		zero;
	int		limit;

	zero = 0x00;
	if (!m)
		limit = COMMENT_LENGTH + 1;
	else
		limit = PROG_NAME_LENGTH + 1;
	limit += limit % 4 ? 4 - limit % 4 : 0;
	i = 0;
	i += cw_write_chars(info, fd_out);
	while (i < limit)
	{
		write(fd_out, &zero, 1);
		i++;
	}
}

void			cw_put_header(int fd_out, header_t bin)
{
	int		magic;

	magic = COREWAR_EXEC_MAGIC;
	magic = cw_to_big_endian(magic);
	write(fd_out, &magic, 4);
	cw_put_info(fd_out, bin.prog_name, NAME);
	bin.prog_size = cw_to_big_endian(bin.prog_size);
	write(fd_out, &bin.prog_size, 4);
	cw_put_info(fd_out, bin.comment, COMMENT);
}
