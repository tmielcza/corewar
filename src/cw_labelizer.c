/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cw_labelizer.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tmielcza <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/02 22:38:54 by tmielcza          #+#    #+#             */
/*   Updated: 2014/02/02 22:38:56 by tmielcza         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */
#include <asm.h>

int				cw_is_label_char(char c)
{
	static char		label_chars[] = LABEL_CHARS;
	int				i;

	i = 0;
	while (label_chars[i] && c != label_chars[i])
		i++;
	if (label_chars[i])
		return (1);
	return (0);
}

int				cw_is_label(char *str)
{
	while (*str && cw_is_label_char(*str))
		str++;
	if (*str == LABEL_CHAR)
		return (1);
	return (0);
}

int				cw_get_label_addr(char *name, t_label *l_labs)
{
	int			i;
	char		*truename;

	i = 0;
	while (cw_is_label_char(name[i]))
		i++;
	truename = ft_strnew(i);
	ft_strncpy(truename, name, i);
	while (l_labs)
	{
		if (!(ft_strcmp(l_labs->name, truename)))
			break ;
		l_labs = l_labs->next;
	}
	free(truename);
	if (l_labs)
		return (l_labs->addr);
	cw_error(nolabel, name);
	return (0);
}

t_label			*cw_add_link(t_label *prev, int size, char *name)
{
	t_label		*new;

	if ((new = (t_label *)malloc(sizeof(t_label))))
	{
		if (prev)
			prev->next = new;
		new->name = ft_strdup(name);
		new->addr = size + 1;
		new->next = NULL;
	}
	else
		cw_error(badmalloc, NULL);
	return (new);
}

char			*cw_labelizer(int size, char *line, t_label *curr)
{
	t_label		*prev;
	int			i;

	i = 0;
	while (line[i] != LABEL_CHAR)
		i++;
	line[i] = '\0';
	while (curr)
	{
		if (!(ft_strcmp(curr->name, line)))
			break ;
		prev = curr;
		curr = curr->next;
	}
	if (curr)
		cw_error(secdeclab, line);
	else
		cw_add_link(prev, size, line);
	return (line + i + 1);
}
