# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gjestin <gjestin@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/01/22 12:19:49 by gjestin           #+#    #+#              #
#    Updated: 2014/02/02 22:35:52 by tmielcza         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #
srcdir = src/
libdir = libft/
includir = include/ libft/

LIBS = -L $(libdir) -lft
INCLUDE = $(addprefix -I,$(includir))
CC = gcc
EXTRAFLAGS = -Wall -Wextra -Werror -g

FLAGSXX = -fstack-protector-all -Wshadow -Wall -Werror -Wextra \
		-Wunreachable-code -Wstack-protector -pedantic-errors \
		-Wfatal-errors -Wstrict-prototypes -Wmissing-prototypes \
		-Wwrite-strings -Wunreachable-code -pedantic \
		-Wunknown-pragmas -Wdeclaration-after-statement \
		-Wold-style-definition -Wmissing-field-initializers \
		-Winline -g -W

CFLAGS = $(INCLUDE) $(EXTRAFLAGS)
VPATH = $(srcdir)

NAME = asm
SRCS =	main.c \
		cw_ass_embler.c \
		cw_put_header.c \
		cw_put_params.c \
		cw_verif.c \
		cw_to_big_endian.c \
		op.c \
		cw_anal_yzer.c \
		cw_labelizer.c \
		cw_error.c \
		cw_free.c \
		cw_generics.c 

OBJS = $(SRCS:.c=.o)

all: $(NAME)

$(NAME): $(OBJS)
	@make -C libft/
	@$(CC) $(CFLAGS) -o $(NAME) $(OBJS) $(LIBS)

clean:
	@make -C libft/ clean
	@rm -f $(OBJS)

fclean:
	@make -C libft/ fclean
	@rm -f $(OBJS) $(NAME)

re: fclean all
